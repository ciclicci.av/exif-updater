﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ExifUpdater.Helpers;

namespace ExifUpdater
{
    public class MetaTag
    {
        //public enum MetaType { Exif, Iptc, Xmp }
        public class DataField
        {
            public string Key { get; set; }
            public static DataField Comment => new DataField { Key = "comment" };
            public static DataField Keywords => new DataField { Key = "keywords" };
        }

        public string FilePath { get; set; }
        public DataField Field { get; set; }
        private List<string> Values { get; set; }
        //private MetaType Type { get; set; }

        private MetaTag(string filePath, List<string> values, DataField field/*, MetaType type*/)
        {
            FilePath = filePath;
            Field = field;
            Values = values;

            //if (field == DataField.Title)
            //    Type = MetaType.Exif;
            //else if (field == DataField.Keywords)
            //    Type = MetaType.Iptc;
        }

        public class MetaTagSingle : MetaTag, IMetaTagSingle
        {
            public string Value { get { return base.Values.FirstOrDefault(); } set { base.Values = new List<string> { value }; } }
            public MetaTagSingle(string filePath, string value, DataField field) : base(filePath, new List<string> { value }, field)
            {
            }
        }

        public class MetaTagMulti : MetaTag, IMetaTagMulti
        {
            public new List<string> Values { get { return base.Values; } set { base.Values = value; } }

            public MetaTagMulti(string filePath, List<string> values, DataField field) : base(filePath, values, field)
            {
            }
        }

        public static MetaTagMulti CreateKeywordsTag(string filePath, List<string> keywords) => new MetaTagMulti(filePath, keywords, DataField.Keywords);
        public static MetaTagSingle CreateCommentTag(string filePath, string title) => new MetaTagSingle(filePath, title, DataField.Comment);
    }

    //public static class ExifTag
    //{
    //    public static MetaTag CreateKeywordsTag(string filePath, string keywords)
    //    {
    //        return new MetaTag(filePath, keywords, MetaTag.DataField.Keywords, MetaTag.MetaType.Exif);
    //    }

    //    public static MetaTag CreateTitleTag(string filePath, string title)
    //    {
    //        return new MetaTag(filePath, title, MetaTag.DataField.Keywords, MetaTag.MetaType.Exif);
    //    }
    //}

    //public static class IptcTag
    //{
    //    public static MetaTag CreateKeywordsTag(string filePath, string keywords)
    //    {
    //        return new MetaTag(filePath, keywords, MetaTag.DataField.Keywords, MetaTag.MetaType.Iptc);
    //    }

    //    public static MetaTag CreateTitleTag(string filePath, string title)
    //    {
    //        return new MetaTag(filePath, title, MetaTag.DataField.Keywords, MetaTag.MetaType.Iptc);
    //    }
    //}

    //public static class XmpTag
    //{
    //    public static MetaTag CreateKeywordsTag(string filePath, string keywords)
    //    {
    //        return new MetaTag(filePath, keywords, MetaTag.DataField.Keywords, MetaTag.MetaType.Xmp);
    //    }

    //    public static MetaTag CreateTitleTag(string filePath, string title)
    //    {
    //        return new MetaTag(filePath, title, MetaTag.DataField.Keywords, MetaTag.MetaType.Xmp);
    //    }
    //}
}
