﻿namespace ExifUpdater
{
    partial class ParametersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parametersGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.keywordsDelimetersLabel = new System.Windows.Forms.Label();
            this.keywordsDelimetersTextBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.filePatternsLabel = new System.Windows.Forms.Label();
            this.filePatternsTextBox = new System.Windows.Forms.TextBox();
            this.parametersGroupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // parametersGroupBox
            // 
            this.parametersGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parametersGroupBox.Controls.Add(this.tableLayoutPanel1);
            this.parametersGroupBox.Location = new System.Drawing.Point(12, 12);
            this.parametersGroupBox.Name = "parametersGroupBox";
            this.parametersGroupBox.Size = new System.Drawing.Size(450, 164);
            this.parametersGroupBox.TabIndex = 0;
            this.parametersGroupBox.TabStop = false;
            this.parametersGroupBox.Text = "Parameters";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.filePatternsTextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.keywordsDelimetersLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.keywordsDelimetersTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.filePatternsLabel, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(444, 141);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // keywordsDelimetersLabel
            // 
            this.keywordsDelimetersLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.keywordsDelimetersLabel.AutoSize = true;
            this.keywordsDelimetersLabel.Location = new System.Drawing.Point(3, 0);
            this.keywordsDelimetersLabel.Name = "keywordsDelimetersLabel";
            this.keywordsDelimetersLabel.Padding = new System.Windows.Forms.Padding(5);
            this.keywordsDelimetersLabel.Size = new System.Drawing.Size(218, 30);
            this.keywordsDelimetersLabel.TabIndex = 0;
            this.keywordsDelimetersLabel.Text = "Keywords delimeters, via space:";
            // 
            // keywordsDelimetersTextBox
            // 
            this.keywordsDelimetersTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keywordsDelimetersTextBox.Location = new System.Drawing.Point(227, 3);
            this.keywordsDelimetersTextBox.Name = "keywordsDelimetersTextBox";
            this.keywordsDelimetersTextBox.Size = new System.Drawing.Size(214, 24);
            this.keywordsDelimetersTextBox.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okButton.Location = new System.Drawing.Point(200, 182);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 27);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // filePatternsLabel
            // 
            this.filePatternsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filePatternsLabel.AutoSize = true;
            this.filePatternsLabel.Location = new System.Drawing.Point(3, 30);
            this.filePatternsLabel.Name = "filePatternsLabel";
            this.filePatternsLabel.Padding = new System.Windows.Forms.Padding(5);
            this.filePatternsLabel.Size = new System.Drawing.Size(218, 111);
            this.filePatternsLabel.TabIndex = 0;
            this.filePatternsLabel.Text = "File patterns (i.e. \'*.jpg\'), via space:";
            // 
            // filePatternsTextBox
            // 
            this.filePatternsTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filePatternsTextBox.Location = new System.Drawing.Point(227, 33);
            this.filePatternsTextBox.Name = "filePatternsTextBox";
            this.filePatternsTextBox.Size = new System.Drawing.Size(214, 24);
            this.filePatternsTextBox.TabIndex = 2;
            // 
            // ParametersForm
            // 
            this.ClientSize = new System.Drawing.Size(474, 221);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.parametersGroupBox);
            this.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(490, 260);
            this.Name = "ParametersForm";
            this.Text = "Parameters";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ParametersForm_FormClosing);
            this.Load += new System.EventHandler(this.ParametersForm_Load);
            this.parametersGroupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox parametersGroupBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label keywordsDelimetersLabel;
        private System.Windows.Forms.TextBox keywordsDelimetersTextBox;
        private System.Windows.Forms.TextBox filePatternsTextBox;
        private System.Windows.Forms.Label filePatternsLabel;
    }
}