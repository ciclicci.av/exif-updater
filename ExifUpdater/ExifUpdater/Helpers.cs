﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater
{
    public static class Helpers
    {
        public enum ProcessResult { Success, Error, NoMetaData, NoFiles };

        public static List<string> GetInputFilesFromPath(string path, List<string> searchPatterns, IExceptionHandler exceptionHandler)
        {
            try
            {
                return EnumerateFiles(path, searchPatterns, SearchOption.AllDirectories).ToList();
            }
            catch (Exception exc)
            {
                exceptionHandler.Handle(exc);
            }

            return new List<string>();
        }

        public static List<string> GetFileNamesFromLine(string line)
        {
            var result = new List<string>();
            var trimmedLine = TrimLineWithFileNames(line);
            if (trimmedLine.Contains(":") && trimmedLine.Where(x => x == ':').Count() == 1)
            {
                var splittedValues = trimmedLine.Split(':').Select(x => x.Trim(new char[] { '"', '\'' }));

                var startFileName = splittedValues.ElementAt(0);
                ThrowIfInvalid(startFileName);
                var endFileName = splittedValues.ElementAt(1);
                ThrowIfInvalid(endFileName);
                result = GetFileNamesForRange(startFileName, endFileName);
            }
            else
                trimmedLine.Split(new char[] { ',' }).ToList().ForEach(word =>
                {
                    word = word.Trim(' ');
                    ThrowIfInvalid(word);
                    //if (word.Any(x => Path.GetInvalidFileNameChars().Contains(x)))
                    //    throw new Exception($"File name \"{word}\" has invalid symbols.");
                    result.Add(word);
                });
            //var invalidName = result.FirstOrDefault(fileName => fileName.Any(x => Path.GetInvalidFileNameChars().Contains(x)));
            //if (invalidName != null)
            //    throw new Exception($"File name \"{invalidName}\" has invalid symbols.");
            return result;
        }

        public static string TrimLineWithFileNames(string line)
        {
            var result = new string(line.Where((c, i) => char.IsLetterOrDigit(c) || c == '"' || c == '\'' || (i > 0 && i < line.Length - 1)).ToArray());
            if (result.Length != line.Length)
                result = TrimLineWithFileNames(result);

            return result;
        }

        public static List<string> GetFileNamesForRange(string startFileName, string endFileName)
        {
            if (startFileName.Length != endFileName.Length)
            {
                // Prepend shorter filename with '0'.
                var shorterFileName = new List<string> { startFileName, endFileName }.OrderBy(x => x.Length).First();
                var longerFileName = new List<string>() { startFileName, endFileName }.Except(new List<string> { shorterFileName }).First();
                longerFileName.Substring(shorterFileName.Length).ToList().ForEach(x => shorterFileName.Insert(0, "0"));
            }

            var values = new List<string>();
            for (var i = startFileName[0]; i <= endFileName[0]; i++)
            {
                if (startFileName.Length > 1)
                    values.AddRange(GetFileNamesForRange(startFileName.Substring(1), endFileName.Substring(1)).Select(name => name.Insert(0, i.ToString())));
                else
                    values.Add(string.Concat(startFileName.Select((c, idx) => idx == 0 ? i : c)));
            }

            return values;
        }

        public static List<string> GetKeywordsFromLine(string line, List<string> delimeters)
        {
            var values = new List<string>(line.Split(delimeters.ToArray(), StringSplitOptions.None).Select(x => x.Trim()));
            return values;
        }

        public static IEnumerable<string> EnumerateFiles(string path, List<string> searchPatterns, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return searchPatterns.AsParallel().SelectMany(searchPattern => Directory.EnumerateFiles(path, searchPattern, searchOption));
        }

        private static void ThrowIfInvalid(string fileName)
        {
            if (fileName.Any(x => Path.GetInvalidFileNameChars().Contains(x)))
                throw new Exception($"File name \"{fileName}\" has invalid symbols.");
        }

        public static T LoadSettingsValue<T>(string value, T defaultValue, IExceptionHandler exceptionHandler)
        {
            T result = defaultValue;

            if (string.IsNullOrEmpty(value))
                return result;

            try
            {
                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(value)))
                {
                    var obj = new BinaryFormatter().Deserialize(ms);
                    if (obj.GetType() == typeof(T))
                        result = (T)obj;
                }
            }
            catch (Exception exc)
            {
                if (exc.GetType() != typeof(System.Runtime.Serialization.SerializationException))
                    exceptionHandler.Handle(exc);
            }

            return result;
        }

        public static string GetBase64String(object item)
        {
            string result = null;
            if (item == null)
                return result;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, item);
                ms.Position = 0;
                byte[] buffer = new byte[(int)ms.Length];
                ms.Read(buffer, 0, buffer.Length);
                result = Convert.ToBase64String(buffer);
            }

            return result;
        }
    }
}
