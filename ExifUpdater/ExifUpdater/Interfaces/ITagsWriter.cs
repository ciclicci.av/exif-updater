﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ExifUpdater.Helpers;

namespace ExifUpdater.Interfaces
{
    public interface ITagsWriter
    {
        void SaveTagsToFile(List<IMetaTag> tags);
    }
}
