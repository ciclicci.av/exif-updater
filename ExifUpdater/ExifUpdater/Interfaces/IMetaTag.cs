﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ExifUpdater.MetaTag;

namespace ExifUpdater.Interfaces
{
    public interface IMetaTag
    {
        string FilePath { get; set; }
        DataField Field { get; set; }
    }
}
