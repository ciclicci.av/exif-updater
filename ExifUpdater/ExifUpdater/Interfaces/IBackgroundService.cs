﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater.Interfaces
{
    public interface IBackgroundService
    {
        void DoAction(Action action);
    }
}
