﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExifUpdater.Interfaces
{
    public interface IExceptionHandler
    {
        void Handle(Exception exc);
        void UpdateLogger(Services services);
    }
}
