﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExifUpdater.Interfaces
{
    public interface IProgressBar
    {
        void Start();
        void SetIndeterminate();
        void SetContinuous(int? totalCount = null);
        void Increase();
        void Stop();
    }
}
