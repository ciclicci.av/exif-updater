﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExifUpdater.Interfaces
{
    public interface IConfiguration
    {
        List<string> KeywordsDelimiters { get; set; }
        List<string> FileSearchPatterns { get; set; }
        Language CurrentLanguage { get; set; }
        void Save();
    }
}
