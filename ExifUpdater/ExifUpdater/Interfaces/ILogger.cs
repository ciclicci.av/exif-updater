﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater.Interfaces
{
    public interface ILogger
    {
        void Add(string text);
        void AddWarning(string text);
        void AddError(string text);
        void AddFilesNotFound(List<string> files);
        void Clear();
        string GetText();
        void UpdateCultureInfo(IConfiguration configuration = null);
    }
}
