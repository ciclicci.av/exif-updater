﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ExifUpdater.Helpers;

namespace ExifUpdater.Interfaces
{
    public interface IInputMetaDataReader
    {
        ILookup<string, IMetaTag> GetMetaDataFromFile(string filePath, List<string> inputFiles, IConfiguration configuration, ILogger log);
    }
}
