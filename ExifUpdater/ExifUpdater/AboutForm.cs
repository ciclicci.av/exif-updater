﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater
{
    public partial class AboutForm : Form
    {
        private ResourceManager _resourceManager;
        private CultureInfo _cultureInfo;

        public AboutForm(CultureInfo cultureInfo, ResourceManager resourceManager)
        {
            Font sysFont = SystemFonts.MessageBoxFont;
            this.Font = new Font(sysFont.Name, sysFont.SizeInPoints, sysFont.Style);

            _cultureInfo = cultureInfo;
            _resourceManager = resourceManager;

            InitializeComponent();

            SetItemsTextValueFromResource();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {

        }

        private void SetItemsTextValueFromResource()
        {
            this.Text = this._resourceManager.GetString("AboutForm", this._cultureInfo);
            this.programmedByLabel.Text = this._resourceManager.GetString("AboutForm_programmedByLabel", this._cultureInfo);
            this.fullNameLabel.Text = this._resourceManager.GetString("AboutForm_fullNameLabel", this._cultureInfo);
            this.okButton.Text = this._resourceManager.GetString("AboutForm_okButton", this._cultureInfo);
        }

        private void emailLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string mailto = string.Format("mailto:{0}", "ciclicci.av@gmail.com");
            System.Diagnostics.Process.Start(mailto);
        }
    }
}
