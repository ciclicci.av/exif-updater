﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using static ExifUpdater.Helpers;

namespace ExifUpdater
{
    public class TagsUpdater
    {
        private ILogger _log;
        private IBackgroundService _backgroundService;
        private IInputMetaDataReader _reader;
        private IProgressBar _progressBar;
        private ITagsWriter _writer;
        private IConfiguration _configuration;
        private IExceptionHandler _exceptionHandler;
        private ResourceManager _resourceManager;
        private CultureInfo _cultureInfo;

        public TagsUpdater(Services services)
        {
            _backgroundService = services.BackgroundService;
            _log = services.Logger;
            _reader = services.MetaDataReader;
            _progressBar = services.ProgressBar;
            _writer = services.TagsWriter;
            _configuration = services.Configuration;
            _exceptionHandler = services.ExceptionHandler;
            _resourceManager = new ResourceManager("ExifUpdater.Resource.Res", typeof(MainForm).Assembly);
            UpdateCultureInfo();
        }

        public void ProcessPath(string inputPath, string keywordsFilePath, Action<ProcessResult> onDone)
        {
            ILookup<string, IMetaTag> metaData = null;
            Action getMetaDataAndFiles = delegate()
            {
                ProcessResult result = ProcessResult.Error;
                try
                {
                    _log.Add(_resourceManager.GetString("Message_StartedProcessing", _cultureInfo));

                    // Load files to update.
                    var files = GetInputFilesFromPath(inputPath, _configuration.FileSearchPatterns, _exceptionHandler);
                    if (files.Count == 0)
                    {
                        result = ProcessResult.NoFiles;
                        return;
                    }

                    _log.Add($"{_resourceManager.GetString("Message_TotalFilesFound", _cultureInfo)}: {files.Count}");

                    // Load meta data.
                    metaData = _reader.GetMetaDataFromFile(keywordsFilePath, files, _configuration, _log);
                    if (metaData.Count == 0)
                    {
                        result = ProcessResult.NoMetaData;
                        return;
                    }

                    var metaDataFiles = metaData.Select(x => x.Key).ToList();
                    _log.Add($"{_resourceManager.GetString("Message_MetadataTotalFiles", _cultureInfo)}: {metaDataFiles.Count}");

                    _log.Add(_resourceManager.GetString("Message_ApplyingMetaData", _cultureInfo));
                    var fileNames = metaData.Select(x => x.Key).ToList();
                    _progressBar.SetContinuous(fileNames.Count);

                    foreach (var fileName in fileNames)
                    {
                        //_backgroundService.DoAction(new Action(() =>
                        //{
                        var metaTags = metaData[fileName].ToList();
                        _writer.SaveTagsToFile(metaTags);
                        //}));

                        _progressBar.Increase();
                    }

                    _log.Add(_resourceManager.GetString("Message_Done", _cultureInfo));
                    result = ProcessResult.Success;
                }
                finally
                {
                    onDone(result);
                }
            };

            _backgroundService.DoAction(getMetaDataAndFiles);
        }

        public void UpdateCultureInfo()
        {
            _cultureInfo = CultureInfo.CreateSpecificCulture(_configuration.CurrentLanguage.Code);
        }
    }
}