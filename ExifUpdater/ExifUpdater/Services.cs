﻿using ExifUpdater.Implementations;
using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater
{
    public class Services
    {
        public IProgressBar ProgressBar { get; set; }
        public IBackgroundService BackgroundService { get; set; }
        public IExceptionHandler ExceptionHandler { get; set; }
        public TagsUpdater TagsUpdater { get; set; }
        public IConfiguration Configuration { get; set; }
        public ILogger Logger { get; set; }
        public IInputMetaDataReader MetaDataReader { get; set; }
        public ITagsWriter TagsWriter { get; set; }

        public Services(ToolStripProgressBar toolStripProgressBar, Action loggerCallback)
        {
            Logger = new MyLogger(loggerCallback, this);
            ExceptionHandler = new MyExceptionHandler(this);
            ProgressBar = new MyProgressBar(toolStripProgressBar);
            BackgroundService = new MyBackgroundService(this);
            Configuration = new MyConfiguration(this);
            UpdateCultureInfo(true);

            MetaDataReader = new MyInputMetaDataReader(this);
            TagsWriter = new MyTagsWriter(this);
            TagsUpdater = new TagsUpdater(this);
        }

        public void UpdateCultureInfo(bool init = false)
        {
            Logger.UpdateCultureInfo(init ? Configuration : null);
            if (init)
                return;

            TagsUpdater.UpdateCultureInfo();
        }
    }
}
