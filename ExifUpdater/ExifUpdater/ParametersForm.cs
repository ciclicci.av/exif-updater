﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater
{
    public partial class ParametersForm : Form
    {
        private IConfiguration _configuration;

        private ResourceManager _resourceManager;
        private CultureInfo _cultureInfo;

        public ParametersForm(Services services)
        {
            Font sysFont = SystemFonts.MessageBoxFont;
            this.Font = new Font(sysFont.Name, sysFont.SizeInPoints, sysFont.Style);

            _configuration = services.Configuration;

            _cultureInfo = CultureInfo.CreateSpecificCulture(_configuration.CurrentLanguage.Code);
            _resourceManager = new ResourceManager("ExifUpdater.Resource.Res", typeof(MainForm).Assembly);

            InitializeComponent();

            SetItemsTextValueFromResource();
            PopulateValues();
        }

        private void SetItemsTextValueFromResource()
        {
            this.Text = this._resourceManager.GetString("ParametersForm", this._cultureInfo);
            this.parametersGroupBox.Text = this._resourceManager.GetString("ParametersForm_parametersGroupBox", this._cultureInfo);
            this.keywordsDelimetersLabel.Text = this._resourceManager.GetString("ParametersForm_keywordsDelimetersLabel", this._cultureInfo);
            this.filePatternsLabel.Text = this._resourceManager.GetString("ParametersForm_filePatternsLabel", this._cultureInfo);
            this.okButton.Text = this._resourceManager.GetString("ParametersForm_okButton", this._cultureInfo);
        }

        private void ParametersForm_Load(object sender, EventArgs e)
        {

        }

        private void PopulateValues()
        {
            keywordsDelimetersTextBox.Text = string.Join(" ", _configuration.KeywordsDelimiters);
            filePatternsTextBox.Text = string.Join(" ", _configuration.FileSearchPatterns);
        }

        private void SaveValues()
        {
            _configuration.KeywordsDelimiters = keywordsDelimetersTextBox.Text.Split(' ').ToList();
            _configuration.FileSearchPatterns = filePatternsTextBox.Text.Split(' ').ToList();
            _configuration.Save();
        }

        private void ParametersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ValuesValid())
                e.Cancel = true;
            else
                SaveValues();
        }

        private bool ValuesValid()
        {
            if (this.keywordsDelimetersTextBox.Text.Where(x => x != ' ').Count() == 0)
            {
                var text = this._resourceManager.GetString("Message_KeywordsDelimetersEmpty", this._cultureInfo);
                var caption = this._resourceManager.GetString("Message_Error", this._cultureInfo);
                MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
