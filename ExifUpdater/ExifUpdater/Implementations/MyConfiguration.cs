﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater.Implementations
{
    class MyConfiguration : IConfiguration
    {
        public List<string> KeywordsDelimiters { get; set; }
        public List<string> FileSearchPatterns { get; set; }
        public Language CurrentLanguage { get; set; }

        private IBackgroundService _backgroundService;
        private IExceptionHandler _exceptionHandler;

        public MyConfiguration(Services services)
        {
            _backgroundService = services.BackgroundService;
            _exceptionHandler = services.ExceptionHandler;

            KeywordsDelimiters = Helpers.LoadSettingsValue(Properties.Settings.Default.KeywordsDelimiters, new List<string> { ",", ";" }, _exceptionHandler);
            FileSearchPatterns = Helpers.LoadSettingsValue(Properties.Settings.Default.FileSearchPatterns, new List<string> { "*.jpg", "*.jpeg", "*.bmp", "*.gif", "*.png", "*.mp4", "*.mov" }, _exceptionHandler);
            CurrentLanguage = Helpers.LoadSettingsValue(Properties.Settings.Default.Language, Language.En, _exceptionHandler);
        }

        public void Save()
        {
            _backgroundService.DoAction(new Action(() =>
            {
                Properties.Settings.Default.KeywordsDelimiters = Helpers.GetBase64String(KeywordsDelimiters);
                Properties.Settings.Default.Language = Helpers.GetBase64String(CurrentLanguage);
                Properties.Settings.Default.Save();
            }));
        }
    }
}
