﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static ExifUpdater.Helpers;

namespace ExifUpdater.Implementations
{
    public class MyInputMetaDataReader : IInputMetaDataReader
    {
        private ILogger _log;
        private IConfiguration _configuration;
        private IExceptionHandler _exceptionHandler;

        public MyInputMetaDataReader(Services services)
        {
            _log = services.Logger;
            _configuration = services.Configuration;
            _exceptionHandler = services.ExceptionHandler;
        }

        public ILookup<string, IMetaTag> GetMetaDataFromFile(string filePath, List<string> inputFiles, IConfiguration configuration, ILogger log)
        {
            var result = new List<IMetaTag>();
            try
            {
                var notFound = new List<string>();
                using (var stream = new StreamReader(filePath))
                {
                    string line;
                    bool titleProcessed = false;
                    bool fileNameProcessed = false;
                    var fileNames = new List<string>();
                    var imageFilePaths = new List<string>();
                    while ((line = stream.ReadLine()) != null)
                    {
                        // Skip empty rows.
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }

                        var title = "";
                        var keywords = new List<string>();

                        if (!fileNameProcessed)
                        {
                            // Contains filename.
                            fileNames = GetFileNamesFromLine(line);
                            fileNameProcessed = true;
                        }
                        else if (!titleProcessed)
                        {
                            // Title row.
                            title = imageFilePaths.Count > 0 ? line.Trim() : title;
                            titleProcessed = true;
                        }
                        else
                        {
                            // Keywords row.
                            keywords = imageFilePaths.Count > 0 ?
                                GetKeywordsFromLine(line, _configuration.KeywordsDelimiters) :
                                keywords;
                            titleProcessed = false;
                            fileNameProcessed = false;
                        }

                        foreach (var imageFilePath in imageFilePaths)
                        {
                            if (titleProcessed)
                                result.Add(MetaTag.CreateCommentTag(imageFilePath, title));
                            else
                                result.Add(MetaTag.CreateKeywordsTag(imageFilePath, keywords));
                        }

                        if (!titleProcessed && !fileNameProcessed)
                        {
                            imageFilePaths = new List<string>();
                            continue;
                        }

                        if (imageFilePaths.Count > 0)
                            continue;

                        foreach (var fileName in fileNames)
                        {
                            var imageFilePath = "";
                            if (TryGetFilePath(fileName, inputFiles, out imageFilePath))
                            {
                                imageFilePaths.Add(imageFilePath);
                            }
                            else
                            {
                                notFound.Add(fileName);
                                continue;
                            }
                        }
                    }
                }

                notFound = notFound.Distinct().ToList();
                if (notFound.Count > 0)
                    _log.AddFilesNotFound(notFound);

            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }

            return result.ToLookup(x => x.FilePath);
        }

        private bool TryGetFilePath(string fileName, List<string> inputFiles, out string filePath)
        {
            filePath = inputFiles.FirstOrDefault(x => x == fileName);
            if (filePath == null)
                filePath = inputFiles.FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == fileName);

            return filePath != null;
        }
    }
}
