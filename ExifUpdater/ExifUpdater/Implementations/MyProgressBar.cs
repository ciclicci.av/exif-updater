﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater.Implementations
{
    public class MyProgressBar : IProgressBar
    {
        private int _progressBarMultiplier;
        private int _itemIndex;
        private ToolStripProgressBar _progressBar;

        public MyProgressBar(ToolStripProgressBar progressBar)
        {
            _progressBar = progressBar;
            _progressBar.Value = 0;
        }

        private static int GetProgressBarMultiplier(int count)
        {
            // Calculate progress bar maximum value and multiplier.
            int progressBarMaximum = count;
            int progressBarMultiplier = 1;
            while (progressBarMaximum > 1000)
            {
                progressBarMaximum /= 10;
                progressBarMultiplier *= 10;
            }

            return progressBarMultiplier;
        }

        public void Increase()
        {
            _itemIndex++;
            _progressBar.GetCurrentParent().BeginInvoke(new Action(() =>
            {
                _progressBar.Value = _itemIndex / _progressBarMultiplier;
            }));
        }

        public void Start()
        {
        }

        public void SetIndeterminate()
        {
            SetStyle(ProgressBarStyle.Marquee);
        }

        public void SetContinuous(int? itemsCount = null)
        {
            do { }
            while (!_progressBar.GetCurrentParent().IsHandleCreated);

            if (itemsCount.HasValue)
            {
                _itemIndex = 0;
                _progressBarMultiplier = GetProgressBarMultiplier(itemsCount.Value);
                SetMaximum(itemsCount.Value / _progressBarMultiplier);
                SetValue(0);
            }

            SetStyle(ProgressBarStyle.Blocks);
        }

        public void Stop()
        {
            SetValue(0);
        }

        private void SetValue(int value)
        {
            if (_progressBar.GetCurrentParent().InvokeRequired)
                _progressBar.GetCurrentParent().BeginInvoke(new Action(() => SetValue(value)));
            else
                _progressBar.Value = value;
        }

        private void SetStyle(ProgressBarStyle style)
        {
            if (_progressBar.GetCurrentParent().InvokeRequired)
                _progressBar.GetCurrentParent().BeginInvoke(new Action(() => SetStyle(style)));
            else
                _progressBar.Style = style;
        }

        private void SetMaximum(int maximum)
        {
            if (_progressBar.GetCurrentParent().InvokeRequired)
                _progressBar.GetCurrentParent().BeginInvoke(new Action(() => SetMaximum(maximum)));
            else
                _progressBar.Maximum = maximum;
        }

        //public static ProgressBar InitProgressBar(ToolStripProgressBar progressBar, ProgressBarStyle style, int itemsCount) => new ProgressBar(progressBar, style, itemsCount);
        //public static IProgressBar InitProgressBarMarquee(ToolStripProgressBar progressBar) => new ProgressBar(progressBar);
    }
}
