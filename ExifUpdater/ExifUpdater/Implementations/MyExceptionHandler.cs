﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExifUpdater.Implementations
{
    public class MyExceptionHandler : IExceptionHandler
    {
        private ILogger _log;

        public MyExceptionHandler(Services services)
        {
            _log = services.Logger;
        }

        public void Handle(Exception exc)
        {
            _log.AddError(exc.InnerException?.Message ?? exc.Message);
        }

        public void UpdateLogger(Services services)
        {
            _log = services.Logger;
        }
    }
}
