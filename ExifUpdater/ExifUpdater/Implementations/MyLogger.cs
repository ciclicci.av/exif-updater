﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater.Implementations
{
    class MyLogger : ILogger
    {
        private List<LogEntry> _entries;
        private Action _callback;
        private ResourceManager _resourceManager;
        private CultureInfo _cultureInfo;
        private IConfiguration _configuration { get; set; }

        public MyLogger(Action callback, Services services)
        {
            _entries = new List<LogEntry>();
            _callback = callback;
            _resourceManager = new ResourceManager("ExifUpdater.Resource.Res", typeof(MainForm).Assembly);
            UpdateCultureInfo();
        }

        public void Add(string text)
        {
            _entries.Add(CreateNormalEntry(text));
            _callback.Invoke();
        }

        public void AddWarning(string text)
        {
            _entries.Add(CreateWarningEntry(text));
            _callback.Invoke();
        }

        public void AddError(string text)
        {
            _entries.Add(CreateErrorEntry(text));
            _callback.Invoke();
        }

        public void AddFilesNotFound(List<string> files)
        {
            string text = string.Join(", ", files.Select(x => $"\"{x}\""));
            text = text.Insert(0, $"{_resourceManager.GetString("Message_FilesForSuchNamesNotFound", _cultureInfo)}: ");

            _entries.Add(CreateWarningEntry(text));
            _callback.Invoke();
        }

        public void Clear()
        {
            _entries.Clear();
        }

        public string GetText()
        {
            return string.Join("\r\n", _entries.Select(x =>
            {
                var prefix = x.Type == LogEntry.EntryType.Error ? $"{_resourceManager.GetString("Message_Error", _cultureInfo)}: " :
                             x.Type == LogEntry.EntryType.Warning ? $"{_resourceManager.GetString("Message_Warning", _cultureInfo)}: " : null;
                prefix = $": {prefix}";
                return $"[{x.DateTime.ToShortDateString()} {x.DateTime.ToLongTimeString()}] {prefix}{x.Text}";
            }));
        }

        public void UpdateCultureInfo(IConfiguration configuration = null)
        {
            if (configuration != null)
                _configuration = configuration;
            _cultureInfo = CultureInfo.CreateSpecificCulture(_configuration?.CurrentLanguage.Code ?? "en");
        }

        private class LogEntry
        {
            public DateTime DateTime { get; set; }
            public EntryType Type { get; set; }
            public string Text { get; set; }

            public enum EntryType { Normal, Warning, Error }

            public LogEntry(DateTime dateTime, EntryType type, string text)
            {
                DateTime = dateTime;
                Type = type;
                Text = text;
            }
        }

        private LogEntry CreateNormalEntry(string text) => new LogEntry(DateTime.Now, LogEntry.EntryType.Normal, text);
        private LogEntry CreateWarningEntry(string text) => new LogEntry(DateTime.Now, LogEntry.EntryType.Warning, text);
        private LogEntry CreateErrorEntry(string text) => new LogEntry(DateTime.Now, LogEntry.EntryType.Error, text);
    }

}
