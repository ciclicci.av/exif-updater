﻿using ExifUpdater.Interfaces;
using NExifTool;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExifUpdater.Implementations
{
    public class MyTagsWriter : ITagsWriter
    {
        private ILogger _log;

        public MyTagsWriter(Services services)
        {
            _log = services.Logger;
        }

        public void SaveTagsToFile(List<IMetaTag> tags)
        {
            var fileName = tags.First().FilePath;
            var et = new ExifTool(new ExifToolOptions() { ExifToolPath = "exiftool(-k).exe" });
            var updatedFileName = "";
            using (var src = new FileStream(fileName, FileMode.Open))
            {
                var operations = new List<NExifTool.Writer.SetOperation>();
                foreach (var tag in tags)
                {
                    if (tag.GetType() == typeof(MetaTag.MetaTagSingle))
                    {
                        var singleTag = (MetaTag.MetaTagSingle)tag;
                        operations.Add(new NExifTool.Writer.SetOperation(new Tag(singleTag.Field.Key, singleTag.Value)));
                    }
                    else if (tag.GetType() == typeof(MetaTag.MetaTagMulti))
                    {
                        var multiTag = (MetaTag.MetaTagMulti)tag;
                        operations.Add(new NExifTool.Writer.SetOperation(new Tag(multiTag.Field.Key, multiTag.Values)));
                    }
                    else
                        throw new Exception("Unknown MetaTag type.");
                }

                updatedFileName = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                et.WriteTagsAsync(src, operations, updatedFileName).Wait();
            }

            File.Delete(fileName);
            File.Copy(updatedFileName, fileName);
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(updatedFileName);
        }
    }
}
