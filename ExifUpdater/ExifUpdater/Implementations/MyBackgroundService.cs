﻿using ExifUpdater.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExifUpdater.Implementations
{
    public class MyBackgroundService : IBackgroundService
    {
        private bool _isProcessRunning;

        private ILogger _log;
        private IProgressBar _progressBar;
        private IExceptionHandler _exceptionHandler;

        public MyBackgroundService(Services services)
        {
            _log = services.Logger;
            _progressBar = services.ProgressBar;
            _exceptionHandler = services.ExceptionHandler;
        }

        public void DoAction(Action action)
        {
            if (IsProcessRunning())
                return;

            _isProcessRunning = true;

            Thread backgroundThread = null;
            try
            {
                _progressBar.Start();

                backgroundThread = new Thread(() =>
                {
                    action();
                    _isProcessRunning = false;
                });
            }
            catch(Exception exc)
            {
                _exceptionHandler.Handle(exc);
                _isProcessRunning = false;
                return;
            }

            backgroundThread.Start();
            //backgroundThread.Join();
        }

        private bool IsProcessRunning()
        {
            if (_isProcessRunning)
                _log.AddError("A process is already running.");
            return _isProcessRunning;
        }
    }
}
