﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExifUpdater
{
    [Serializable]
    public class Language
    {
        public Language(string code, string name)
        {
            this.Code = code;
            this.Name = name;
        }

        public string Code { get; set; }
        public string Name { get; set; }

        // Initialize languages.
        public static Language En = new Language("en", "English");
        public static Language Ru = new Language("ru", "Русский");

        public static List<Language> GetLanguages() => new List<Language> { En, Ru };

        public static Language GetLanguage(string code) => GetLanguages().FirstOrDefault(x => x.Code == code);

        public override bool Equals(object other)
        {
            return this == (other as Language);
        }

        // This is the method that must be implemented to conform to the 
        // IEquatable contract
        public bool Equals(Language other)
        {
            return this == other;
        }

        public static bool operator ==(Language x, Language y)
        {
            if (object.ReferenceEquals(x, y))
            {
                return true;
            }

            if (object.ReferenceEquals(x, null) ||
                object.ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Code == y.Code && x.Name == y.Name;
        }

        public static bool operator !=(Language x, Language y)
        {
            return !(x == y);
        }

        public override int GetHashCode()
        {
            return 17 * Code.GetHashCode() + 23 * Name.GetHashCode();
        }
    }
}
