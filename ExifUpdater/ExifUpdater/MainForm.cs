﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using ExifUpdater.Interfaces;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExifUpdater.Implementations;
using System.Runtime.Serialization.Formatters.Binary;

namespace ExifUpdater
{
    public partial class MainForm : Form
    {
        private Services _services;

        private TagsUpdater _tagsUpdater;
        private IBackgroundService _backgroundService;
        private IExceptionHandler _exceptionHandler;
        private ILogger _log;
        private IConfiguration _configuration;

        private ResourceManager _resourceManager;
        private CultureInfo _cultureInfo;

        public MainForm()
        {
            Font sysFont = SystemFonts.MessageBoxFont;
            this.Font = new Font(sysFont.Name, sysFont.SizeInPoints, sysFont.Style);

            InitializeComponent();

            _services = new Services(toolStripProgressBar1, RefreshLogWindow);

            _tagsUpdater = _services.TagsUpdater;
            _backgroundService = _services.BackgroundService;
            _exceptionHandler = _services.ExceptionHandler;
            _log = _services.Logger;
            _configuration = _services.Configuration;

            _resourceManager = new ResourceManager("ExifUpdater.Resource.Res", typeof(MainForm).Assembly);
            _cultureInfo = CultureInfo.CreateSpecificCulture(_configuration.CurrentLanguage.Code);

            SetItemsTextValueFromResource();

            var buildDate = BuildDateTime.GetLinkerTimestampUtc(System.Reflection.Assembly.GetExecutingAssembly());
            this.versionToolStripStatusLabel.Text = $"{buildDate.Day.ToString("D2")}.{buildDate.Month.ToString("D2")}.{buildDate.Year.ToString("D4")} {buildDate.Hour.ToString("D2")}:{buildDate.Minute.ToString("D2")}:{buildDate.Second.ToString("D2")} UTC";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void aboutEXIFUpdaterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new AboutForm(_cultureInfo, _resourceManager).ShowDialog();
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoSelectFolder();
        }

        private void toolStripProgressBar1_Click(object sender, EventArgs e)
        {

        }

        private void DoSelectFolder()
        {
            try
            {
                if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                    this.inputFolderTextBox.Text = this.folderBrowserDialog1.SelectedPath;
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                this.applyTagsButton.Enabled = false;
                //this.selectFolderToolStripMenuItem.Enabled = false;
                //this.openKeywordsFileToolStripMenuItem.Enabled = false;
                this.parametersToolStripMenuItem.Enabled = false;
                this.englishToolStripMenuItem.Enabled = false;
                this.русскийToolStripMenuItem.Enabled = false;

                // Check input data.
                if (!Directory.Exists(inputFolderTextBox.Text))
                {
                    string text = this._resourceManager.GetString("Message_SpecifiedDirectoryNotFound", _cultureInfo);
                    string caption = this._resourceManager.GetString("Message_Warning", _cultureInfo);
                    MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (!File.Exists(metaDataFilePathTextBox.Text))
                {
                    string text = this._resourceManager.GetString("Message_SpecifiedMetaDataFileNotFound", _cultureInfo);
                    string caption = this._resourceManager.GetString("Message_Warning", _cultureInfo);
                    MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                Action<Helpers.ProcessResult> processResult = delegate(Helpers.ProcessResult result)
                {
                    if (result == Helpers.ProcessResult.NoFiles)
                    {
                        string text = this._resourceManager.GetString("Message_NoFilesToProcessInSpecifiedDirectory", _cultureInfo);
                        string caption = this._resourceManager.GetString("Message_Warning", _cultureInfo);
                        MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        _log.Add(text);
                    }
                    else if (result == Helpers.ProcessResult.NoMetaData)
                    {
                        string text = this._resourceManager.GetString("Message_NoMetaDataFoundInSpecifiedFile", _cultureInfo);
                        string caption = this._resourceManager.GetString("Message_Warning", _cultureInfo);
                        MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        _log.Add(text);
                    }

                    this.BeginInvoke(new Action(() =>
                    {
                        this.applyTagsButton.Enabled = true;
                        //this.selectFolderToolStripMenuItem.Enabled = true;
                        //this.openKeywordsFileToolStripMenuItem.Enabled = true;
                        this.parametersToolStripMenuItem.Enabled = true;
                        this.englishToolStripMenuItem.Enabled = true;
                        this.русскийToolStripMenuItem.Enabled = true;
                    }));
                };

                _tagsUpdater.ProcessPath(inputFolderTextBox.Text, metaDataFilePathTextBox.Text, processResult);
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void RefreshLogWindow(/*string log*/)
        {
            if (this.logTextBox.InvokeRequired)
            {
                var result = this.logTextBox.BeginInvoke(new Action(() => RefreshLogWindow()));
            }
            else
            {
                this.logTextBox.Clear();
                this.logTextBox.AppendText(_log.GetText());
            }
        }

        private void SetItemsTextValueFromResource()
        {
            this.fileToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_fileToolStripMenuItem", _cultureInfo);
            this.selectFolderToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_selectFolderToolStripMenuItem", _cultureInfo);
            this.openKeywordsFileToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_openKeywordsFileToolStripMenuItem", _cultureInfo);
            this.exitToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_exitToolStripMenuItem", _cultureInfo);

            this.settingsToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_settingsToolStripMenuItem", _cultureInfo);
            this.languageToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_languagesToolStripMenuItem", _cultureInfo);
            this.parametersToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_parametersToolStripMenuItem", _cultureInfo);
            this.englishToolStripMenuItem.Text = this.englishToolStripMenuItem.Text;
            this.русскийToolStripMenuItem.Text = this.русскийToolStripMenuItem.Text;

            this.helpToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_helpToolStripMenuItem", _cultureInfo);
            this.aboutToolStripMenuItem.Text = this._resourceManager.GetString("MainForm_aboutToolStripMenuItem", _cultureInfo);

            this.inputDataGroupBox.Text = this._resourceManager.GetString("MainForm_inputDataGroupBox", _cultureInfo);
            this.folderLabel.Text = this._resourceManager.GetString("MainForm_folderLabel", _cultureInfo);
            this.keywordsLabel.Text = this._resourceManager.GetString("MainForm_keywordsLabel", _cultureInfo);
            this.applyTagsButton.Text = this._resourceManager.GetString("MainForm_applyTagsButton", _cultureInfo);
            this.logGroupBox.Text = this._resourceManager.GetString("MainForm_logGroupBox", _cultureInfo);

            this.progressToolStripStatusLabel.Text = this._resourceManager.GetString("MainForm_progressToolStripStatusLabel", _cultureInfo);
            this.buildToolStripStatusLabel.Text = this._resourceManager.GetString("MainForm_buildToolStripStatusLabel", _cultureInfo);
        }

        private void selectFileButton_Click(object sender, EventArgs e)
        {
            DoSelectKeywords();
        }

        private void DoSelectKeywords()
        {
            try
            {
                this.openFileDialog1.FileName = null;
                if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
                    this.metaDataFilePathTextBox.Text = this.openFileDialog1.FileName;
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void selectFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoSelectFolder();
        }

        private void openKeywordsFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoSelectKeywords();
        }

        private void SelectLanguage(Language languageCode)
        {
            _configuration.CurrentLanguage = languageCode;
            _cultureInfo = CultureInfo.CreateSpecificCulture(languageCode.Code);

            SetLanguageMenuItems();
            SetItemsTextValueFromResource();

            _configuration.Save();
            _services.UpdateCultureInfo();
        }

        private void SetLanguageMenuItems()
        {
            if (_configuration.CurrentLanguage == Language.En)
            {
                this.englishToolStripMenuItem.Checked = true;
                this.русскийToolStripMenuItem.Checked = false;
            }
            else if (_configuration.CurrentLanguage == Language.Ru)
            {
                this.englishToolStripMenuItem.Checked = false;
                this.русскийToolStripMenuItem.Checked = true;
            }
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.englishToolStripMenuItem.Checked)
                {
                    SelectLanguage(Language.En);
                }
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void русскийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.русскийToolStripMenuItem.Checked)
                {
                    SelectLanguage(Language.Ru);
                }
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void keywordsDelimeterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                new ParametersForm(_services).ShowDialog();
            }
            catch (Exception exc)
            {
                _exceptionHandler.Handle(exc);
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }
    }
}
